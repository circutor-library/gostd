// Copyright (c) 2023 Circutor S.A. All rights reserved.

package gostd

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"io"
	"math"
	"time"
)

const TimeSTDOffset = 2010

const (
	CodeTypeLong   = 0x0000
	CodeTypeUlong  = 0x1000
	CodeTypeChar   = 0x2000
	CodeTypeUchar  = 0x3000
	CodeTypeInt    = 0x4000
	CodeTypeUint   = 0x5000
	CodeTypeFloat  = 0x6000
	CodeTypeDouble = 0x7000
	CodeTypeEnergy = 0x8000
	CodeTypeSample = 0xE000
	CodeTypeString = 0xF000
)

//nolint:cyclop
func writeToSTD(dataToWrite interface{}, w io.Writer) {
	byteStream := make([]byte, 0, 4)
	switch d := dataToWrite.(type) {
	case byte:
		byteStream = byteStream[:1]
		byteStream[0] = d
	case int:
		byteStream = byteStream[:4]
		binary.BigEndian.PutUint32(byteStream, uint32(d))
	case eventType:
		byteStream = byteStream[:2]
		binary.BigEndian.PutUint16(byteStream, uint16(d))
	case eventDescriptor:
		byteStream = byteStream[:2]
		binary.BigEndian.PutUint16(byteStream, uint16(d))
	case uint:
		byteStream = byteStream[:4]
		binary.BigEndian.PutUint32(byteStream, uint32(d))
	case []byte:
		byteStream = d
	case string:
		byteStream = []byte(d)
	case time.Time:
		byteStream = TimeToSTDTime(d)
	case uint16, uint32, int8, int16, int32, float32, float64:
		err := binary.Write(w, binary.BigEndian, d)
		if err != nil {
			panic(fmt.Sprintf("binary write in STD failed: %v", err.Error()))
		}

		return
	default:
		panic("unexpected data type to write in STD")
	}

	_, werr := w.Write(byteStream)
	if werr != nil {
		panic(fmt.Sprintf("unable to write data: %v", werr.Error()))
	}
}

func writeVariableCode(code VariableCode, codeType VariableType, fileBuffer *bytes.Buffer) {
	dataVarCode := uint16(codeType) | uint16(code)
	writeToSTD(dataVarCode, fileBuffer)
}

func createSTDHeader(data StandardFileData, w io.Writer) error {
	buffer := &bytes.Buffer{}

	writeToSTD(data.SerialNum, buffer)
	writeToSTD(data.FirmwareVer, buffer)
	writeToSTD(data.ControlType, buffer)
	writeToSTD(calculateHeaderLength(data), buffer)
	writeToSTD(calculateRecordLength(data), buffer)
	writeToSTD(uint16(len(data.HeaderVariables)), buffer)

	for _, headerVar := range data.HeaderVariables {
		writeVariableCode(headerVar.Code, headerVar.Type, buffer)
	}

	writeToSTD(uint16(len(data.RecordVariables[0])), buffer)

	for _, recordVar := range data.RecordVariables[0] {
		writeVariableCode(recordVar.Code, recordVar.Type, buffer)
	}

	for _, headerVar := range data.HeaderVariables {
		writeToSTD(headerVar.Value, buffer)
	}

	writeToSTD(calculateCheckSum(buffer.Bytes()), buffer)

	_, err := w.Write(buffer.Bytes())
	if err != nil {
		return fmt.Errorf("write failed: %w", err)
	}

	return nil
}

func createEVAHeader(data StandardEventsData, w io.Writer) error {
	buffer := &bytes.Buffer{}

	writeToSTD(data.SerialNum, buffer)
	writeToSTD(data.FirmwareVer, buffer)
	writeToSTD(data.Filename, buffer)
	writeToSTD(uint16(0), buffer)
	writeToSTD(uint16(TimeSTDOffset), buffer)
	// Padding to make header even
	writeToSTD([]byte{0}, buffer)
	writeToSTD(calculateCheckSum(buffer.Bytes()), buffer)

	_, err := w.Write(buffer.Bytes())
	if err != nil {
		return fmt.Errorf("write failed: %w", err)
	}

	return nil
}

func createEVARegister(reg EventRegister, w io.Writer) error {
	buffer := &bytes.Buffer{}
	writeToSTD(TimeToSTDTime(reg.EventTime), buffer)
	writeToSTD(reg.EventType, buffer)
	writeToSTD(reg.EventDescriptor, buffer)
	// Padding to make register even
	writeToSTD([]byte{0}, buffer)
	writeToSTD(calculateCheckSum(buffer.Bytes()), buffer)

	_, err := w.Write(buffer.Bytes())
	if err != nil {
		return fmt.Errorf("write failed: %w", err)
	}

	return nil
}

func calculateCheckSum(buf []byte) byte {
	var sum uint32
	for _, b := range buf {
		sum += uint32(b)
	}

	return byte((sum % 256) ^ 0xFF)
}

func TimeToSTDTime(date time.Time) []byte {
	timeTemp := make([]byte, 4)

	timeTemp[0] = (byte(date.Year()-TimeSTDOffset) << 2) + (byte(date.Month()) >> 2)
	timeTemp[1] = (byte(date.Month()) << 6) + (byte(date.Day()) << 1) + (byte(date.Hour()) >> 4)
	timeTemp[2] = (byte(date.Hour()) << 4) + (byte(date.Minute()) >> 2)
	timeTemp[3] = (byte(date.Minute()) << 6) + byte(date.Second())

	return timeTemp
}

func calculateHeaderLength(data StandardFileData) uint16 {
	headerLen := 26 // SerialNum+FirmwareVer 16 bytes, control type, header length, record length, header elements, record elements 2 bytes each

	sizeMap := map[int]int{
		CodeTypeLong:   4,
		CodeTypeUlong:  4,
		CodeTypeChar:   1,
		CodeTypeUchar:  1,
		CodeTypeInt:    2,
		CodeTypeUint:   2,
		CodeTypeFloat:  4,
		CodeTypeDouble: 8,
		CodeTypeEnergy: 8,
		CodeTypeSample: 0,
		CodeTypeString: 0,
	}

	for _, headerVar := range data.HeaderVariables {
		headerLen += 2
		headerLen += sizeMap[int(headerVar.Type)]

		if headerVar.Type == CodeTypeSample || headerVar.Type == CodeTypeString {
			varLen := math.Pow(2, float64((headerVar.Code&0x0F00)>>8))
			headerLen += int(varLen)
		}
	}

	if len(data.RecordVariables) == 0 {
		return 0
	}

	// All records should be same length, so we use first one to calculate length
	for range data.RecordVariables[0] {
		headerLen += 2
	}

	switch data.ControlType {
	case ControlTypeCheckSum:
		headerLen++
	case ControlTypeCRC16:
		headerLen += 2
	case ControlTypeCRC32:
		headerLen += 4
	}

	return uint16(headerLen)
}

func calculateRecordLength(data StandardFileData) uint16 {
	headerLen := 0
	sizeMap := map[int]int{
		CodeTypeLong:   4,
		CodeTypeUlong:  4,
		CodeTypeChar:   1,
		CodeTypeUchar:  1,
		CodeTypeInt:    2,
		CodeTypeUint:   2,
		CodeTypeFloat:  4,
		CodeTypeDouble: 8,
		CodeTypeEnergy: 8,
		CodeTypeSample: 0,
		CodeTypeString: 0,
	}

	if len(data.RecordVariables) == 0 {
		return 0
	}

	// All records should be same length, so we use first one to calculate length
	recordVar := data.RecordVariables[0]
	for _, rec := range recordVar {
		headerLen += sizeMap[int(rec.Type)]

		if rec.Type == CodeTypeSample || rec.Type == CodeTypeString {
			varLen := math.Pow(2, float64((rec.Code&0x0F00)>>8))
			headerLen += int(varLen)
		}
	}

	switch data.ControlType {
	case ControlTypeCheckSum:
		headerLen++
	case ControlTypeCRC16:
		headerLen += 2
	case ControlTypeCRC32:
		headerLen += 4
	}

	return uint16(headerLen)
}

// Copyright (c) 2023 Circutor S.A. All rights reserved.

package gostd

import (
	"bytes"
	"fmt"
	"io"
	"time"
)

const (
	ControlTypeCheckSum = iota
	ControlTypeCRC16
	ControlTypeCRC32
)

type (
	eventType       uint16
	VariableType    uint16
	VariableCode    uint16
	eventDescriptor uint16
)

type StandardVariable struct {
	Type  VariableType
	Code  VariableCode
	Value interface{}
}

type EventRegister struct {
	EventTime       time.Time
	EventType       eventType
	EventDescriptor eventDescriptor
}

type StandardFileData struct {
	SerialNum       string // max 10 chars
	FirmwareVer     string // max 6 chars
	ControlType     uint16
	HeaderVariables []StandardVariable
	RecordVariables [][]StandardVariable
}

type StandardEventsData struct {
	SerialNum   string // max 10 chars
	FirmwareVer string // max 6 chars
	Filename    string // max 12 chars
	EventsData  []EventRegister
}

func GenerateSTD(data StandardFileData, w io.Writer) error {
	buffer := &bytes.Buffer{}

	err := createSTDHeader(data, buffer)
	if err != nil {
		return fmt.Errorf("STD header creation failed: %w", err)
	}

	for _, rec := range data.RecordVariables {
		recordStart := buffer.Len()

		for _, e := range rec {
			writeToSTD(e.Value, buffer)
		}

		recordEnd := buffer.Len()

		writeToSTD(calculateCheckSum(buffer.Bytes()[recordStart:recordEnd]), buffer)
	}

	_, err = w.Write(buffer.Bytes())

	if err != nil {
		return fmt.Errorf("STD file write failed: %w", err)
	}

	return nil
}

func GenerateEvents(data StandardEventsData, w io.Writer) error {
	buffer := &bytes.Buffer{}

	err := createEVAHeader(data, buffer)
	if err != nil {
		return fmt.Errorf("EVA header creation failed: %w", err)
	}

	for _, e := range data.EventsData {
		err = createEVARegister(e, buffer)
		if err != nil {
			return err
		}
	}

	_, err = w.Write(buffer.Bytes())

	if err != nil {
		return fmt.Errorf("EVA file write failed: %w", err)
	}

	return nil
}

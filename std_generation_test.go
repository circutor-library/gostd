// Copyright (c) 2023 Circutor S.A. All rights reserved.

package gostd_test

import (
	"bytes"
	"testing"
	"time"

	"github.com/benbjohnson/clock"
	"github.com/sebdah/goldie/v2"
	"github.com/stretchr/testify/assert"
	std "gitlab.com/circutor-library/gostd"
)

func TestStdGeneration(t *testing.T) {
	bufferFile := &bytes.Buffer{}

	c := clock.NewMock()
	c.Set(time.Date(2023, time.February, 21, 3, 6, 42, 0, time.Local))

	eventData := []std.EventRegister{
		{
			EventTime: c.Now(), EventType: 20, EventDescriptor: 4,
		},
		{
			EventTime: c.Now().Add(1 * time.Hour), EventType: 20, EventDescriptor: 8,
		},
		{
			EventTime: c.Now().Add(-1 * time.Hour), EventType: 20, EventDescriptor: 12,
		},
		{
			EventTime: c.Now().Add(-24 * time.Hour), EventType: 20, EventDescriptor: 16,
		},
	}
	data := std.StandardEventsData{
		SerialNum:   "ROFL123456",
		FirmwareVer: "1.2.34",
		Filename:    "EVATest1.EVA",
		EventsData:  eventData,
	}

	err := std.GenerateEvents(data, bufferFile)
	assert.NoError(t, err)

	compareWithGoldenSample(t, bufferFile.Bytes())
}

func compareWithGoldenSample(t *testing.T, report []byte) {
	t.Helper()

	g := goldie.New(t, goldie.WithNameSuffix(".golden.EVA"))
	g.Assert(t, t.Name(), report)
}
